# WorkersApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

### Installation

App requires [Node.js](https://nodejs.org/) v8.9.4,  to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd workers-app
$ npm install
```

### Development

Open your favorite Terminal and run these commands.

First Tab:
```sh
$ mongod
```

Second Tab:
```sh
$ mongoimport --db workers --collection workers --drop --file data.json
```

Third Tab:
```sh
$ node server
```

4th Tab:
```sh
$ ng serve
```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
