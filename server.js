// Express
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const cors = require('cors')
const app = express();

// Parsers
app.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// Body Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

//Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

// Morgan
let morgan = require("morgan");
app.use(morgan('dev'));

// Mongo Database
let mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/workers');
let WorkersSchema = new mongoose.Schema({
    name: { type: String, require: true },
    position: { type: String, require: true },
    office: { type: String, require: true },
    ext: { type: Number, require: true },
    startDate: { type: Date, require: true },
    salary: { type: Number, require: true }
})
mongoose.model("workers", WorkersSchema);
let Workers = mongoose.model("workers");


// Routes
// Get Workers
app.get("/workers", (req, res, next) => {
    console.log("Server > GET '/workers' ");
    Workers.find({}, (err, workers)=>{
        return res.json(workers);
    })
})
// Get Worker
app.get("/workers/:_id", (req, res, next) => {
    console.log("Server > GET '/workers/:_id' > _id ", req.params._id);
    Workers.find({_id: req.params._id}, (err, workers)=>{
        return res.json(workers);
    })
})

// Create Worker
app.post("/workers", (req, res, next) => {
    console.log("Server > POST '/workers' > workers ", req.body);
    delete req.body._id
    Workers.create(req.body, (err, workers)=>{
        if (err) 
            return res.json(err)
        else 
            return res.json(workers)
    })
})
// Destroy Worker
app.delete("/workers/:_id", (req, res, next) => {
    console.log("Server > DELETE '/workers/:_id' > _id ", req.params._id);
    Workers.deleteOne({_id: req.params._id}, (err, rawData)=>{
        if (err) return res.json(err)
        else return res.json(true)
    })
})

// Update Worker
app.put("/workers/:_id", (req, res, next) => {
    console.log("Server > PUT '/workers/:_id' > _id ", req.params._id);
    console.log("Server > PUT '/workers/:_id' > workers ", req.body);
    Workers.update({_id:req.params._id}, req.body, (err, rawData)=>{
        if (err) return res.json(err)
        else return res.json(true)
    })  
})

// Send all other requests to the Angular app
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/*app.all("*", (req,res,next) => {
    res.sendfile(path.resolve("./public/dist/index.html"))
})*/

/*const port = process.env.PORT || '3000';
app.set('port', port);*/

//const server = http.createServer(app);

// Server Listening @ 1337
//app.listen(1337, ()=> console.log("Server running at 1337")); 
app.listen(port, () => console.log(`Running on localhost:${port}`));