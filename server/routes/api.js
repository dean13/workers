const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

// Connect
const connection = (closure) => {
    return MongoClient.connect('mongodb://localhost:27017/workers', (err, db) => {
        if (err) return console.log(err);

        closure(db);
    });
};

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

// Get users
router.get('/workers', (req, res) => {
    connection((db) => {
        db.collection('workers')
            .find()
            .toArray()
            .then((workers) => {
                response.data = workers[0];
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.post('/workers', (req, res, next) => {
    console.log(req.body);

    let item = {
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        ext: req.body.ext,
        startDate: req.body.startDate,
        salary: req.body.salary
    };
    connection((db) => {
        db.collection('workers').insertOne(item, (err, res) => {
            if (err) console.log(err);
            else console.log("Worker added");
        })
    });
});


module.exports = router;