import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { WorkersListComponent } from "./workers/workers-list/workers-list.component";

const APP_ROUTES : Route[] = [
    {path: '', pathMatch: 'full', redirectTo: 'workers'},
    {path: 'workers', component: <any>WorkersListComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(APP_ROUTES)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}