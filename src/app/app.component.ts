import { Component, OnInit } from '@angular/core';
import { Workers } from './workers/workers.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'app';
  worker: Workers;

  ngOnInit() {

  }

  receiveWorker($event) {
    this.worker = $event;
  }
}
