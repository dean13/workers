import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import {DataTableModule } from 'angular-4-data-table-bootstrap-4';
//import { WorkersModule } from './workers/workers.module';
import { WorkersService } from './workers/workers.service';

//import { WorkersComponent } from './workers.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReactiveFormsModule } from '@angular/forms';
import {RouterModule, ActivatedRoute} from '@angular/router';
import { WorkersListComponent } from './workers/workers-list/workers-list.component';
import { WorkersFormComponent } from './workers/workers-form/workers-form.component';
import { AppRoutingModule } from './app-routing.module';
import { WorkersRoutingModule } from './workers/workers-routing.module';
import { WorkersDetailsComponent } from './workers/workers-details/workers-details.component';
import { WorkerResolve } from './workers/workers-resolve.service';
import { CoreModule } from './core-module/core.module';
import { HeaderComponent } from './shared-module/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    WorkersListComponent, 
    WorkersFormComponent,
    WorkersDetailsComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    HttpModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    DataTableModule,
    CommonModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    AppRoutingModule,
    WorkersRoutingModule,
    RouterModule,
    CoreModule
  ],
  exports: [
    WorkersListComponent,
    WorkersFormComponent,
    WorkersDetailsComponent
  ],
  providers: [HttpClientModule, WorkersService, WorkerResolve],
  bootstrap: [AppComponent]
})
export class AppModule { }
