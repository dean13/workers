import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkersListComponent } from './workers-list/workers-list.component';
import { WorkersFormComponent } from './workers-form/workers-form.component';
//import { WorkersComponent } from './workers.component';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReactiveFormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import { WorkersDetailsComponent } from './workers-details/workers-details.component';


@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    ReactiveFormsModule,
  ],
  exports: [
    WorkersListComponent,
    WorkersFormComponent,
    //WorkersComponent
  ],
  declarations: [
    WorkersListComponent, 
    WorkersFormComponent, WorkersDetailsComponent, /*WorkersComponent*/
  ],
})
export class WorkersModule { }
