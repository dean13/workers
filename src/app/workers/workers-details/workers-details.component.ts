import { Component, OnInit } from '@angular/core';
import { WorkersService } from '../workers.service';
import { Workers } from '../workers.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-workers-details',
  templateUrl: './workers-details.component.html',
  styleUrls: ['./workers-details.component.less']
})
export class WorkersDetailsComponent implements OnInit {

  worker: Workers;
  workersForm: FormGroup;

  constructor(private workersService : WorkersService,
              private route : ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.loadWorker();
    this.workersForm = this.buildWorkersForm();
  }

  buildWorkersForm() {
    return this.formBuilder.group({
      name: [this.worker.name, Validators.compose([Validators.required, Validators.minLength(3)])],
      position: [this.worker.position, Validators.compose([Validators.required, Validators.minLength(3)])],
      office: [this.worker.office, Validators.compose([Validators.required, Validators.minLength(3)])],
      ext: [this.worker.ext, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
      startDate: [this.worker.startDate, Validators.required],
      salary: [this.worker.salary, Validators.required]
    });
  }

  addWorker() {
    //this.createNewWorkerEvent.emit(this.workersForm.value);
    this.workersService.addWorker(this.workersForm.value).subscribe(() => {
      this.router.navigate(['/workers']);
    })
  }

  updateWorker() {
    this.workersService.updateWorker(this.worker._id, this.workersForm.value).subscribe(() => {
      this.router.navigate(['/workers']);
    })
  }

  loadWorker() {
    this.worker = this.route.snapshot.data['worker'][0];
    //console.log(this.route.snapshot.params._id);
    //const _id = this.route.snapshot.params._id;
    //console.log(_id);

    /*this.workersService.getWorker(_id).subscribe((worker) => {
      console.log(worker);
      this.worker = worker;
    })*/
  }

}
