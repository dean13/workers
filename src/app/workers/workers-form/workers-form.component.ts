import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Workers } from '../workers.model';
import { WorkersService } from '../workers.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-workers-form',
  templateUrl: './workers-form.component.html',
  styleUrls: ['./workers-form.component.less']
})
export class WorkersFormComponent implements OnInit {

  worker: Workers;
  //@Input() worker: Workers;
  //private _worker: Workers;

  workersForm: FormGroup;

  constructor(private formBuilder: FormBuilder, 
              private workersService: WorkersService,
              private route : ActivatedRoute,
              private router: Router) {
  }

  /*ngOnChanges(changes: SimpleChanges) {
    //console.log(this.worker);
    
    const worker: SimpleChange = changes.worker;
    //console.log('prev value: ', worker.previousValue);
    console.log('got name: ', worker.currentValue);
    this._worker = worker.currentValue;
    this.workersForm = this.buildWorkersForm();
  }*/

  ngOnInit() {
    this.loadWorker();
    this.workersForm = this.buildWorkersForm();
  }

  buildWorkersForm() {
    return this.formBuilder.group({
      name: [this.worker, Validators.compose([Validators.required, Validators.minLength(3)])],
      position: [this.worker, Validators.compose([Validators.required, Validators.minLength(3)])],
      office: [this.worker, Validators.compose([Validators.required, Validators.minLength(3)])],
      ext: [this.worker, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
      startDate: [this.worker, Validators.required],
      salary: [this.worker, Validators.required]
    });
  }

  addWorker() {
    //this.createNewWorkerEvent.emit(this.workersForm.value);
    this.workersService.addWorker(this.workersForm.value).subscribe(() => {
      this.router.navigate(['/workers']);
    })
  }

  loadWorker() {
    console.log(this.route.snapshot);
    this.worker = this.route.snapshot.data['worker'];
    //console.log(this.route.snapshot.params._id);
    //const _id = this.route.snapshot.params._id;
    //console.log(_id);

    /*this.workersService.getWorker(_id).subscribe((worker) => {
      console.log(worker);
      this.worker = worker;
    })*/
  }
}
