import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Workers } from '../workers.model';
import { WorkersService } from '../workers.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-workers-list',
  templateUrl: './workers-list.component.html',
  styleUrls: ['./workers-list.component.less']
})
export class WorkersListComponent implements OnInit {

  @ViewChild('myTable') table: any;

  workers: Workers[] = [];
  worker: Workers;
  workersForm: FormGroup;

  columns = [
    { name: 'name' },
    { name: 'position' },
    { name: 'office' },
    { name: 'ext' },
    { name: 'startDate' },
    { name: 'salary' },
  ];
  selected = [];
  temp = [];
  expanded: any = {};
  timeout: any;
  
  //message: string = "HELLO WORLD";

  @Output() workerEvent = new EventEmitter<Workers>();

  constructor(private workersService: WorkersService,
              private router : Router,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.loadWorkers();
    this.workersForm = this.buildWorkersForm();
  }

  buildWorkersForm() {
    return this.formBuilder.group({
      name: [this.worker, Validators.compose([Validators.required, Validators.minLength(3)])],
      position: [this.worker, Validators.compose([Validators.required, Validators.minLength(3)])],
      office: [this.worker, Validators.compose([Validators.required, Validators.minLength(3)])],
      ext: [this.worker, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4)])],
      startDate: [this.worker, Validators.required],
      salary: [this.worker, Validators.required]
    });
  }

  sendWorker() {
    this.workerEvent.emit(this.selected[0]);
  }

  addWorker() {
    //this.createNewWorkerEvent.emit(this.workersForm.value);
    this.workersService.addWorker(this.workersForm.value).subscribe(() => {
      this.loadWorkers();
      this.workersForm.reset();
      //this.router.navigate(['/workers']);
    })
  }

  loadWorkers(): void {
    this.workersService.getWorkers().subscribe((workers) => {
      //console.log(workers);
      this.temp = [...workers];
      this.workers = workers;
    })
  }

  removeWorker(_id: number) {
    this.workersService.removeWorker(_id).subscribe(() => {
      this.loadWorkers();
      this.router.navigate(['/workers']);
    })
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.workers = temp;
    // Whenever the filter changes, always go back to the first page
    //this.table.offset = 0;
  }

  onPage(event) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }

  /*toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }*/

  /*onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }*/

  onSelect({ selected }) {
    //console.log(selected[0]._id);
    this.router.navigate(['/workers', selected[0]._id]);
    this.sendWorker();
    //console.log('Select Event', selected, this.selected);
  }

}
