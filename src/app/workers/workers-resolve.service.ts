import { WorkersService } from "./workers.service";
import { Resolve } from "@angular/router/src/interfaces";
import { Workers } from "./workers.model";
import { ActivatedRouteSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";

@Injectable()
export class WorkerResolve implements Resolve<Workers>{

    constructor (private workersService : WorkersService) {}
    
    resolve(route : ActivatedRouteSnapshot) {
        return this.workersService.getWorker(route.params._id);
    }
}