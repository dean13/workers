import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { WorkersDetailsComponent } from "./workers-details/workers-details.component";
import { WorkerResolve } from "./workers-resolve.service";

const WORKERS_ROUTES : Route[] = [
    {
        path: 'workers/:_id', 
        component: <any>WorkersDetailsComponent,
        resolve: {worker: WorkerResolve}
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(WORKERS_ROUTES)
    ],
    exports: [
        RouterModule
    ]
})

export class WorkersRoutingModule {}