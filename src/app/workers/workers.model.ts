export interface Workers {
    _id: number;
    name: string;
    position: string;
    office: string;
    ext: number;
    startDate: Date;
    salary: number;
}