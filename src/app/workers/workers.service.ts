import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Workers} from './workers.model';

@Injectable()
export class WorkersService {

  private apiUrl = "http://localhost:3000/workers/";

  constructor(private http: Http) { }

  addWorker(worker: Workers) : Observable<Workers> {
    return this.http.post(this.apiUrl, worker)
      .map((res) => res.json())
  }

  removeWorker(_id: number) : Observable<Workers> {
    return this.http.delete(this.apiUrl + `${_id}`)
      .map((res) => res.json())
  }

  updateWorker(id: number, worker) : Observable<Workers> {
    return this.http.put(this.apiUrl + `${id}`, worker)
      .map((res) => res.json())
  }

  getWorker(_id : number) : Observable<Workers> {
    return this.http.get(this.apiUrl + `${_id}`)  
      .map((res) => res.json())
  }

  getWorkers(): Observable<Workers[]> {
    return this.http.get(this.apiUrl)
      .map((res) => res.json())
  }

}
